/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.test;

import java.util.Scanner;

/**
 *
 * @author Gxz32
 */
public class SubnetCalculator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the IP address: ");
        String ipAddress = scanner.nextLine();

        System.out.print("Enter the subnet mask: ");
        String subnetMask = scanner.nextLine();

        String[] ipParts = ipAddress.split("\\.");
        String[] maskParts = subnetMask.split("\\.");

        int[] ipInts = new int[4];
        int[] maskInts = new int[4];

        for (int i = 0; i < 4; i++) {
            ipInts[i] = Integer.parseInt(ipParts[i]);
            maskInts[i] = Integer.parseInt(maskParts[i]);
        }

        int[] networkAddress = new int[4];
        int[] broadcastAddress = new int[4];
        int[] firstHost = new int[4];
        int[] lastHost = new int[4];

        for (int i = 0; i < 4; i++) {
            networkAddress[i] = ipInts[i] & maskInts[i];
            broadcastAddress[i] = (ipInts[i] & maskInts[i]) | (~maskInts[i] & 255);
            firstHost[i] = i == 3 ? networkAddress[i] + 1 : networkAddress[i];
            lastHost[i] = i == 3 ? broadcastAddress[i] - 1 : broadcastAddress[i];
        }

        System.out.println("Network Address: " + arrayToString(networkAddress));
        System.out.println("Broadcast Address: " + arrayToString(broadcastAddress));
        System.out.println("Host Address Range: " +arrayToString(firstHost) + " - "+arrayToString(lastHost));
    }

    public static String arrayToString(int[] array) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < array.length; i++) {
            result.append(array[i]);
            if (i < array.length - 1) {
                result.append(".");
            }
        }
        return result.toString();
    }
}
